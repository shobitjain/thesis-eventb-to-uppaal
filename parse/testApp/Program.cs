﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace testApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string data = @"fn_tr() fn_vss() fn_vss() fn_Lst_VSS() fn_vss() fn_tr() fn_VSS_i()";
            IEnumerable<string> collection = Regex.Matches(data, @"([^\s]+)")
                 .OfType<Match>()
                 .Select(m => m.Groups[0].Value)
                 .Distinct();
            Console.WriteLine(data.Replace("&&&&&&", "&&"));
            Console.ReadKey(true);
        }
    }
}

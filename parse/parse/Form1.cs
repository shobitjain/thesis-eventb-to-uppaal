﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using parse;
using Microsoft.VisualBasic;


namespace Parse
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void button1_Click(object sender, EventArgs e)
        {
            ScMachineFile ScM = null;
            XmlSerializer serializer = new XmlSerializer(typeof(ScMachineFile));
            MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(sourceFileData));
            ScM = (ScMachineFile)serializer.Deserialize(memStream);
            Dictionary<string, List<string>> statesHolder = new Dictionary<string, List<string>>();
            List<int> locationIds = new List<int>();
            Dictionary<string, Location> locations = new Dictionary<string, Location>();
            Nta nta = new Nta();
            string initialState = string.Empty;
            string templateName = !string.IsNullOrEmpty(sourceFileName) ? sourceFileName : "template"; ;
            Template template = new Template();
            template.Name = new Name();
            template.Name.Text = templateName;
            if (ScM != null)
            {
                Dictionary<string, string> typeDefVariables = new Dictionary<string, string>();
                Dictionary<string, string> pTypeVariables = new Dictionary<string, string>();
                List<string> pTypeList = new List<string>();
                string parameterizedVar = string.Empty;
                string parameterizedVarValue = string.Empty;
                int parameterizedValue = 0;
                string variableType = string.Empty;
                #region Varaibles part
                if (ScM.ScVariable != null && ScM.ScVariable.Count > 0)
                {
                    string uppDecVar = string.Empty;
                    string uppSCVarUnknownDataType = string.Empty;
                    string postParameterizedVar = string.Empty;
                    foreach (ScVariable variable in ScM.ScVariable)
                    {
                        if (variable.Type == Constant.eventToUppalVariable.BOOL)
                        {
                            uppDecVar = string.IsNullOrEmpty(variable.Type) ? string.Empty :
                                uppDecVar + (variable.Type).ToLower() + " " + variable.Name + ";\n";
                            nta.Declaration = nta.Declaration + uppDecVar;
                        }
                        else if (variable.Type == Constant.eventToUppalVariable.INT)
                        {
                            uppDecVar = string.IsNullOrEmpty(variable.Type) ? string.Empty :
                                uppDecVar + "int " + variable.Name + ";\n";
                            nta.Declaration = nta.Declaration + uppDecVar;
                        }
                        else if (variable.Type.Contains(Constant.eventToUppalVariable.X))
                        {
                            if (string.IsNullOrEmpty(parameterizedVar))
                            {
                                parameterizedVar = variable.Type.Substring(variable.Type.IndexOf('(') + 1,
                                    variable.Type.IndexOf('×') - variable.Type.IndexOf('(') - 1);
                                parameterizedVarValue = Interaction.InputBox("Enter number of instances", "Instances", "Enter number here 0-9");
                                if (!IsValidLength(parameterizedVarValue))
                                {
                                    richTextBox1.Text = "Input length is 2 at max and numeric";
                                    return;
                                }
                                if (!IsNumeric(parameterizedVarValue))
                                {
                                    richTextBox1.Text = "Input number must be numeric";
                                    return;
                                }
                                parameterizedValue = Convert.ToInt32(parameterizedVarValue);
                                uppDecVar = "\nconst int " + parameterizedVar + " = " +
                                    (!string.IsNullOrEmpty(parameterizedVarValue) && parameterizedVarValue.Length <= 2 && parameterizedVarValue.Length >= 0 ? parameterizedVarValue : "5")
                                    + ";\n";
                                uppDecVar = uppDecVar + "\ntypedef int [0," + parameterizedVar + " - 1] id_t;\n";
                                template.Parameter = "const id_t id";
                            }

                            postParameterizedVar = variable.Type.Substring(variable.Type.IndexOf('×') + 1,
                                    variable.Type.IndexOf(')') - variable.Type.IndexOf('×') - 1);
                            if (postParameterizedVar.Equals(Constant.eventToUppalVariable.BOOL))
                                uppDecVar = uppDecVar + postParameterizedVar.ToLower() + " " + variable.Name + "[" + parameterizedVar + "];\n";
                            else if (postParameterizedVar.Equals(Constant.eventToUppalVariable.INT))
                            { uppDecVar = uppDecVar + "id_t " + variable.Name + "[id_t];\n"; }
                            else
                            {
                                typeDefVariables.Add(variable.Name, postParameterizedVar);
                            }
                            nta.Declaration = nta.Declaration + uppDecVar;
                        }
                        else
                        {
                            if (variable.Type.Contains(Constant.eventToUppalVariable.P))
                            {
                                variableType = variable.Type.Replace(Constant.eventToUppalVariable.P, "");
                                variableType = variableType.Replace("(", "");
                                variableType = variableType.Replace(")", "");
                                pTypeVariables.Add(variable.Name, variableType);
                            }
                            else
                            {
                                uppSCVarUnknownDataType = string.IsNullOrEmpty(variable.Type) ? string.Empty : uppDecVar + (variable.Type)
                                        + " " + variable.Name + ";\n";
                                nta.Declaration = nta.Declaration + uppSCVarUnknownDataType;
                            }
                        }
                        uppDecVar = string.Empty;
                    }
                    // Adding P types (if any) to nta declarations
                    foreach (var pTypeVariable in pTypeVariables)
                    {
                        if (!string.IsNullOrEmpty(parameterizedVar) && pTypeVariable.Value.Equals(parameterizedVar))
                        {
                            uppDecVar = uppDecVar + "int " + pTypeVariable.Key + "[" + pTypeVariable.Value + "];\n";
                        }
                        else
                        {
                            if (!pTypeList.Contains(pTypeVariable.Value))
                            {
                                pTypeList.Add(pTypeVariable.Value);
                                uppDecVar = uppDecVar + "const int " + pTypeVariable.Value + " = " + parameterizedValue + ";\n" +
    "int " + pTypeVariable.Key + "[" + pTypeVariable.Value + "];\n";
                            }
                            else
                            {
                                uppDecVar = uppDecVar + "int " + pTypeVariable.Key + "[" + pTypeVariable.Value + "];\n";
                            }
                        }
                    }
                    nta.Declaration = nta.Declaration + uppDecVar;
                    uppDecVar = string.Empty;
                    //********************************************
                }
                #endregion

                #region Constants part
                string eventToUppconstant = string.Empty;
                string firstConstantName = string.Empty;
                string holder = string.Empty;
                string tempVarNameHolder = string.Empty;
                Dictionary<string, string> complexGuardHolder = new Dictionary<string, string>();
                string guardExpressionHolder = string.Empty;
                Dictionary<string, string> complexAssignmentHolder = new Dictionary<string, string>();
                string assignmentExpressionHolder = string.Empty;
                List<string> icAxiomsList = new List<string>();
                List<string> icConstantsList = new List<string>();
                string icCarrierSetName = string.Empty;
                string icCarrierSetStatment = string.Empty;
                string icCarrierSetRange = string.Empty;
                string icConstantValue = string.Empty;
                string constants = string.Empty;
                MatchCollection matchCollectionConstants, matchCollectionConstantsWithValues;
                foreach (ScInternalContext scInternalContext in ScM.ScInternalContext)
                {
                    foreach (ScConstant scConstant in scInternalContext.ScConstant)
                    {
                        if (scConstant.Type.Equals(Constant.eventToUppalVariable.INT))
                        {
                            foreach (ScAxiom scAxiom in scInternalContext.ScAxiom)
                            {
                                if (scAxiom.Predicate.Contains(scConstant.Name))
                                {
                                    eventToUppconstant = eventToUppconstant + "const int " +
                                        scAxiom.Predicate + ";\n";
                                }
                            }
                        }
                    }
                    foreach (ScAxiom scAxiom in scInternalContext.ScAxiom)
                    {
                        if (!scAxiom.Label.ToLower().Contains("states"))
                            icAxiomsList.Add(scAxiom.Predicate);
                    }

                    foreach (string icAxiomPredicate in icAxiomsList)
                    {
                        if (icAxiomPredicate.Contains("partition"))
                        {
                            matchCollectionConstants = Regex.Matches(icAxiomPredicate, Constant.eventToUppalVariable.axiomPartitionPattern);
                            icCarrierSetName = icAxiomPredicate.Substring(icAxiomPredicate.IndexOf('(') + 1, (icAxiomPredicate.IndexOf(',') - icAxiomPredicate.IndexOf('(')) - 1);
                            foreach (string axiomHolder in icAxiomsList)
                            {
                                if (axiomHolder.Contains("⤖") && axiomHolder.Contains(icCarrierSetName))
                                {
                                    icCarrierSetRange = axiomHolder.Substring(axiomHolder.IndexOf("⤖") + 1, (axiomHolder.Length - axiomHolder.IndexOf("⤖")) - 1);
                                }
                            }
                            if (matchCollectionConstants.Count > 0)
                            {
                                foreach (string axiomHolder in icAxiomsList)
                                {
                                    firstConstantName = matchCollectionConstants[0].Value;
                                    if (axiomHolder.Contains(firstConstantName) && axiomHolder.Contains("↦"))
                                    {
                                        matchCollectionConstantsWithValues = Regex.Matches(axiomHolder, Constant.eventToUppalVariable.axiomCarrierSetConstantValuePattern);
                                        for (int j = 0; j < matchCollectionConstantsWithValues.Count; j++)
                                            icConstantsList.Add(matchCollectionConstantsWithValues[j].Value);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(icCarrierSetName) && !string.IsNullOrEmpty(icCarrierSetRange))
                    {
                        icCarrierSetRange = icCarrierSetRange.Replace("‥", ",");
                        icCarrierSetStatment = "\ntypedef int[" + icCarrierSetRange + "] " + icCarrierSetName + ";";
                    }
                    if (icConstantsList.Count > 0)
                    {
                        foreach (var item in icConstantsList)
                        {
                            tempVarNameHolder = item.Replace("↦", "=");
                            tempVarNameHolder = "const int " + tempVarNameHolder + ";";
                            constants = constants + "\n" + tempVarNameHolder;
                        }
                        for (int i = 0; i < parameterizedValue; i++)
                        {
                            holder = string.IsNullOrEmpty(holder) ? firstConstantName : holder + "," + firstConstantName;
                        }
                    }

                    eventToUppconstant = eventToUppconstant + (!string.IsNullOrEmpty(constants) ? constants : "") + (!string.IsNullOrEmpty(icCarrierSetStatment) ? icCarrierSetStatment : "");
                    foreach (var item in typeDefVariables)
                    {
                        if (item.Value.Equals(icCarrierSetName))
                        {
                            eventToUppconstant = eventToUppconstant + "\n" + item.Value + " " + item.Key + "[" + parameterizedVar + "]={" + holder + "};\n";
                        }
                    }
                    icCarrierSetName = string.Empty;
                    icCarrierSetRange = string.Empty;
                    icConstantsList.Clear();
                    icAxiomsList.Clear();
                    nta.Declaration = nta.Declaration + eventToUppconstant;
                    eventToUppconstant = string.Empty;
                }
                #endregion

                #region Generate query file from invariants
                if (ScM.ScInvariant != null && ScM.ScInvariant.Count > 0)
                {
                    string eventToUppaalInva = string.Empty;
                    foreach (ScInvariant scInvariant in ScM.ScInvariant)
                    {
                        if (scInvariant.Theorem == "true")
                        {
                            eventToUppaalInva = scInvariant.Predicate;
                            string queryFile = @"//queryFile_" + DateTime.Today.ToLongTimeString().ToString() + ".q";
                            var path1 = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + queryFile;
                            File.WriteAllText(path1, eventToUppaalInva);
                        }
                    }
                }
                #endregion

                #region Events part
                string edgeGuards = string.Empty, edgeGuard = string.Empty, edgeActions = string.Empty;
                string fromState = string.Empty;
                string toState = string.Empty;
                if (ScM.ScEvent != null && ScM.ScEvent.Count > 0)
                {
                    string transitionName = string.Empty;
                    bool hasEnter = false, hasInit = false; //alreadyAdded=false;
                    string temporaryLabelHolder = string.Empty;
                    string replaceSelfPById = string.Empty;
                    string assignmentValue = string.Empty;
                    string assignmentKey = string.Empty;
                    string assignmentHolder = string.Empty;
                    string guardsArgument = string.Empty;
                    string edgeSelections = string.Empty;
                    Regex regex = new Regex(Constant.eventToUppalVariable.wordMatchPattern);
                    Match result = Match.Empty;
                    foreach (ScEvent scEvent in ScM.ScEvent)
                    {
                        if (scEvent.Label.ToUpper() == "INITIALISATION")
                        {
                            foreach (ScAction scAction in scEvent.ScAction)
                            {
                                temporaryLabelHolder = !string.IsNullOrEmpty(scAction.Label) ? scAction.Label.ToLower() : string.Empty;
                                if (!string.IsNullOrEmpty(temporaryLabelHolder) && !hasInit && temporaryLabelHolder.Contains("init_") && temporaryLabelHolder.Contains("state"))
                                {
                                    initialState = scAction.Assignment;
                                    initialState = scAction.Assignment.Substring(initialState.LastIndexOf('{') + 1, (initialState.LastIndexOf('}') - initialState.LastIndexOf('{')) - 1);
                                    hasInit = true;
                                }
                            }
                        }

                        else
                        {
                            foreach (ScParameter scParameter in scEvent.ScParameter)
                            {
                                edgeSelections = string.IsNullOrEmpty(edgeSelections) ? scParameter.Name + " : id_t" :
                                                    edgeSelections + ", " + scParameter.Name + " : id_t";
                            }

                            foreach (ScGuard scGuard in scEvent.ScGuard)
                            {
                                if (scGuard.Label.Contains("isnotin_"))
                                {
                                    if (!hasInit)
                                    {
                                        initialState = scGuard.Label;
                                        initialState = scGuard.Label.Substring(initialState.IndexOf('_') + 1);
                                        fromState = initialState;
                                    }
                                    else
                                    {
                                        fromState = scGuard.Label;
                                        fromState = scGuard.Label.Substring(fromState.IndexOf('_') + 1);
                                    }
                                }
                                else if (string.IsNullOrEmpty(fromState) && scGuard.Label.Contains("isin_"))
                                {
                                    fromState = scGuard.Label;
                                    fromState = scGuard.Label.Substring(fromState.IndexOf('_') + 1);
                                }
                                else if (!scGuard.Label.Contains("isin_") && !scGuard.Label.Contains("isnotin_"))
                                {
                                    result = regex.Match(scGuard.Predicate);

                                    if (string.IsNullOrEmpty(edgeGuards))
                                    {
                                        edgeGuards = result.Success ? scGuard.Predicate.Replace(result.Value, "[id]") : scGuard.Predicate;
                                        if (IsComplex(edgeGuards))
                                        {
                                            edgeGuards = "fn_" + GetComplexFunctionName(edgeGuards) + "()";
                                            if (!complexGuardHolder.ContainsKey(edgeGuards))
                                            {
                                                complexGuardHolder.Add(edgeGuards, string.Format("//{0}\n", scGuard.Predicate)); //edgeGuards is used instead of edgeGuard because initially edgeGuards is empty
                                            }
                                        }
                                        else
                                            edgeGuards = edgeGuards + "\n";
                                    }
                                    else
                                    {
                                        replaceSelfPById = result.Success ? scGuard.Predicate.Replace(result.Value, "[id]") : scGuard.Predicate;
                                        edgeGuard = replaceSelfPById;
                                        if (IsComplex(edgeGuard))
                                        {
                                            edgeGuard = "fn_" + GetComplexFunctionName(edgeGuard) + "()";
                                            edgeGuards = edgeGuards + " " + edgeGuard;
                                            if (!complexGuardHolder.ContainsKey(edgeGuard))
                                            {
                                                complexGuardHolder.Add(edgeGuard, string.Format("//{0}\n", scGuard.Predicate));
                                            }
                                            else
                                            {
                                                guardExpressionHolder = complexGuardHolder[edgeGuard];
                                                guardExpressionHolder = guardExpressionHolder + string.Format("//{0}\n", scGuard.Predicate);
                                                complexGuardHolder[edgeGuard] = guardExpressionHolder;
                                            }
                                        }
                                        else
                                        {
                                            edgeGuards = edgeGuards + " " + replaceSelfPById;
                                            replaceSelfPById = string.Empty;
                                        }
                                    }
                                }

                            }
                            edgeGuards = edgeGuards.Replace("=", "==");
                            edgeGuards = edgeGuards.Replace("TRUE", "true");
                            edgeGuards = edgeGuards.Replace("FALSE", "false");
                            edgeGuards = edgeGuards.Replace("≠", "!=");
                            edgeGuards = edgeGuards.Replace("⇒", " imply ");
                            edgeGuards = edgeGuards.Replace("≤", "<=");
                            edgeGuards = edgeGuards.Replace("≥", ">=");
                            edgeGuards = edgeGuards.Replace("∨", "||");
                            edgeGuards = edgeGuards.Replace("¬", "!");
                            edgeGuards = edgeGuards.Replace("−", "-");

                            edgeGuards = RemoveDuplicate(edgeGuards, "&&\n");
                            foreach (ScAction scAction in scEvent.ScAction)
                            {
                                if (string.IsNullOrEmpty(toState) && scAction.Label.Contains("enter_"))
                                {
                                    toState = scAction.Label;
                                    toState = toState.Substring(toState.IndexOf('_') + 1);
                                    hasEnter = true;
                                }
                                else if (!scAction.Label.Contains("enter_") && !scAction.Label.Contains("leave_"))
                                {
                                    assignmentKey = string.Empty;
                                    assignmentValue = string.Empty;
                                    assignmentHolder = scAction.Assignment.Replace("≔", ":=");
                                    if (string.IsNullOrEmpty(edgeActions))
                                    {
                                        edgeActions = assignmentHolder + " \n";
                                        if (edgeActions.Contains('↦'))
                                        {
                                            assignmentKey = edgeActions.Substring(0, (edgeActions.IndexOf(':')) - 1);
                                            assignmentValue = edgeActions.Substring(edgeActions.LastIndexOf('↦') + 2, (edgeActions.LastIndexOf('}') - edgeActions.LastIndexOf('↦')) - 2);
                                            edgeActions = assignmentKey + "[id]:=" + assignmentValue + "\n";
                                        }
                                        else if (IsComplex(edgeActions))
                                        {
                                            edgeActions = "fn_" + edgeActions.Substring(0, edgeActions.IndexOf(" ")) + "()";
                                            if (!complexAssignmentHolder.ContainsKey(edgeActions))
                                                complexAssignmentHolder.Add(edgeActions, string.Format("//{0}\n", scAction.Assignment));
                                            edgeActions = edgeActions + "\n";
                                        }
                                    }
                                    else
                                    {
                                        edgeActions = edgeActions + ", ";
                                        if (!string.IsNullOrEmpty(assignmentHolder) && assignmentHolder.Contains('↦'))
                                        {

                                            assignmentKey = assignmentHolder.Substring(0, (assignmentHolder.IndexOf(':')) - 1);
                                            assignmentValue = assignmentHolder.Substring(assignmentHolder.LastIndexOf('↦') + 2, (assignmentHolder.LastIndexOf('}') - assignmentHolder.LastIndexOf('↦')) - 2);
                                            edgeActions = edgeActions + assignmentKey + "[id]:=" + assignmentValue + "\n";

                                        }
                                        else if (IsComplex(assignmentHolder))
                                        {
                                            assignmentKey = "fn_" + assignmentHolder.Substring(0, assignmentHolder.IndexOf(" ")) + "()";
                                            edgeActions = edgeActions + assignmentKey;
                                            if (!complexAssignmentHolder.ContainsKey(assignmentKey))
                                            {
                                                complexAssignmentHolder.Add(assignmentKey, string.Format("//{0}\n", scAction.Assignment));
                                            }
                                            else
                                            {
                                                assignmentExpressionHolder = complexAssignmentHolder[assignmentKey];
                                                assignmentExpressionHolder = assignmentExpressionHolder + string.Format("//{0}\n", scAction.Assignment);
                                                complexAssignmentHolder[assignmentKey] = assignmentExpressionHolder;
                                            }
                                            edgeActions = edgeActions + "\n";
                                        }
                                        else
                                        {
                                            edgeActions = edgeActions + assignmentHolder + "\n";
                                        }

                                    }

                                    assignmentHolder = string.Empty;
                                }

                            }
                            edgeActions = edgeActions.Replace("TRUE", "true");
                            edgeActions = edgeActions.Replace("FALSE", "false");
                            edgeActions = edgeActions.Replace("≠", "!=");
                            edgeActions = edgeActions.Replace("⇒", " imply ");
                            edgeActions = edgeActions.Replace("≤", "<=");
                            edgeActions = edgeActions.Replace("≥", ">=");
                            edgeActions = edgeActions.Replace("<+", "=");
                            edgeSelections = edgeSelections.Replace("BOOL", "bool");
                            edgeSelections = edgeSelections.Replace("INT", "int");
                            statesHolder.Add(scEvent.Label,
                                new List<string> { fromState, hasEnter ? toState : fromState, edgeGuards, edgeActions, edgeSelections });
                            hasEnter = false;
                            toState = string.Empty;
                            fromState = string.Empty;
                        }
                        edgeActions = string.Empty;
                        edgeGuards = string.Empty;
                        edgeSelections = string.Empty;
                    }
                }
                List<string> fromList = new List<string>();
                List<string> toList = new List<string>();
                List<string> itemList = new List<string>();
                string state = string.Empty;

                // New logic added to loop over events again due to last minute change
                if (ScM.ScEvent != null && ScM.ScEvent.Count > 0)
                {
                    foreach (ScEvent scEvent in ScM.ScEvent)
                    {
                        fromList.Clear();
                        toList.Clear();
                        state = string.Empty;
                        foreach (ScGuard scGuard in scEvent.ScGuard)
                        {
                            if (scGuard.Label.Contains("isin_"))
                            {
                                state = scGuard.Label;
                                state = state.Substring(state.IndexOf('_') + 1);
                                if (!fromList.Contains(state))
                                    fromList.Add(state);
                            }
                        }
                        if (fromList.Count > 0)
                            fromList.RemoveAt(0);
                        foreach (ScAction scAction in scEvent.ScAction)
                        {
                            if (scAction.Label.Contains("leave_"))
                            {
                                state = scAction.Label;
                                state = state.Substring(state.IndexOf('_') + 1);
                                if (!fromList.Contains(state))
                                    fromList.Add(state);
                            }
                            else if (scAction.Label.Contains("enter_"))
                            {
                                state = scAction.Label;
                                state = state.Substring(state.IndexOf('_') + 1);
                                if (!toList.Contains(state))
                                    toList.Add(state);
                            }
                        }
                        if (fromList.Count > 0 && toList.Count > 0)
                        {
                            foreach (string item in fromList)
                            {
                                if (statesHolder.ContainsKey(scEvent.Label))
                                {
                                    statesHolder[scEvent.Label].Add(item);
                                    statesHolder[scEvent.Label].Add(toList.Last());
                                    statesHolder[scEvent.Label].Add(string.Empty);
                                    statesHolder[scEvent.Label].Add(string.Empty);
                                    statesHolder[scEvent.Label].Add(string.Empty);
                                }
                            }
                        }
                    }
                }
                //*********************************************************************
                List<Location> seperateLocationList = new List<Location>();
                List<Transition> seperateTransitionList = new List<Transition>();
                List<Template> templateList = new List<Template>();
                List<Transition> transitions = new List<Transition>();
                Location location = null;
                Transition transition = null;
                Label label = null;
                int id = 0, indexJumper = 5;
                foreach (var item in statesHolder)
                {
                    for (int index = 0; index < item.Value.Count / 5; index++)
                    {
                        if (!locations.ContainsKey(item.Value[index * indexJumper + 0]))
                        {
                            location = new Location();
                            id = GetNewLocationId(locationIds);
                            locationIds.Add(id);
                            location.Id = "id" + Convert.ToString(id);
                            location.Name = new Name();
                            location.Name.Text = item.Value[index * indexJumper + 0];
                            locations.Add(item.Value[index * indexJumper + 0], location);
                        }
                        if (!locations.ContainsKey(item.Value[index * indexJumper + 1]))
                        {
                            location = new Location();
                            id = GetNewLocationId(locationIds);
                            locationIds.Add(id);
                            location.Name = new Name();
                            location.Name.Text = item.Value[index * indexJumper + 1];
                            location.Id = "id" + Convert.ToString(id);
                            locations.Add(item.Value[index * indexJumper + 1], location);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(initialState))
                {
                    template.Init = new Init();
                    template.Init.Ref = locations.ContainsKey(initialState) ? locations[initialState].Id : string.Empty;
                }
                foreach (var item in statesHolder)
                {
                    for (int index = 0; index < item.Value.Count / 5; index++)
                    {
                        transition = new Transition();
                        if (index == 0)
                        {
                            transition.Label = new List<Label>();
                            label = new Label();
                            label.Kind = "comments";
                            label.Text = item.Key;
                            transition.Label.Add(label);

                            label = new Label();
                            label.Kind = "guard";
                            label.Text = item.Value[2];
                            transition.Label.Add(label);

                            label = new Label();
                            label.Kind = "assignment";
                            label.Text = item.Value[3];
                            transition.Label.Add(label);

                            label = new Label();
                            label.Kind = "select";
                            label.Text = item.Value[4];
                            transition.Label.Add(label);

                            transition.Source = new Source();
                            transition.Source.Ref = locations[item.Value[0]].Id;
                            transition.Target = new Target();
                            transition.Target.Ref = locations[item.Value[1]].Id;
                            transitions.Add(transition);
                        }
                        else
                        {
                            transition.Source = new Source();
                            transition.Source.Ref = locations[item.Value[index * indexJumper + 0]].Id;
                            transition.Target = new Target();
                            transition.Target.Ref = locations[item.Value[index * indexJumper + 1]].Id;
                            transitions.Add(transition);
                        }
                    }
                }

                template.Location = new List<Location>();
                foreach (var item in locations)
                {
                    if (!string.IsNullOrEmpty(item.Key))
                        template.Location.Add(item.Value);
                    else
                    {
                        if (!seperateLocationList.Contains(location))
                            seperateLocationList.Add(item.Value);
                    }
                }
                template.Transition = new List<Transition>();
                template.Transition.AddRange(transitions);

                string functionDeclarations = string.Empty;
                if (string.IsNullOrEmpty(template.Declaration))
                    foreach (var item in complexAssignmentHolder)
                    {
                        functionDeclarations = @"bool " + item.Key + " {\n" + item.Value + "return true;\n}\n" + functionDeclarations;
                    }
                template.Declaration = template.Declaration + functionDeclarations;
                functionDeclarations = string.Empty;
                foreach (var item in complexGuardHolder)
                {
                    functionDeclarations = @"bool " + item.Key + " {\n" + item.Value + "return true;\n}\n" + functionDeclarations;
                }
                template.Declaration = template.Declaration + functionDeclarations;
                template.Declaration = "clock local;\n" + template.Declaration;
                templateList.Add(template);
                string matchedValue = string.Empty;
                IEnumerable<string> matchCollection = null;
                List<string> declaredFunctions = new List<string>();
                foreach (var locationItem in seperateLocationList)
                {
                    functionDeclarations = string.Empty;
                    foreach (Transition transitionItem in transitions)
                    {
                        if (transitionItem.Source.Ref.Equals(locationItem.Id) && transitionItem.Target.Ref.Equals(locationItem.Id))
                        {
                            if (transitionItem.Label != null)
                            {
                                label = new Label();
                                label.Kind = "select";
                                label.Text = "id:id_t";
                                foreach (var item in transitionItem.Label)
                                {
                                    if (item.Kind == "select")
                                    {
                                        label.Text = string.IsNullOrEmpty(item.Text) ? "id:id_t" : item.Text + ", id:id_t";
                                    }
                                    else if (item.Kind == "assignment")
                                    {
                                        matchCollection = Regex.Matches(item.Text, Constant.eventToUppalVariable.functionDeclarationpattern)
                                                                    .OfType<Match>()
                                                                    .Select(m => m.Groups[0].Value)
                                                                    .Distinct();
                                        foreach (string match in matchCollection)
                                        {
                                            matchedValue = match.Trim();
                                            if (complexAssignmentHolder.ContainsKey(matchedValue) && !declaredFunctions.Contains(matchedValue))
                                            {
                                                functionDeclarations = functionDeclarations + "\nbool " + matchedValue + "{\n" + complexAssignmentHolder[matchedValue] + "return true;\n}";
                                                declaredFunctions.Add(matchedValue);
                                            }
                                        }
                                    }
                                    else if (item.Kind == "guard")
                                    {
                                        matchCollection = Regex.Matches(item.Text, Constant.eventToUppalVariable.functionDeclarationpattern)
                                                                    .OfType<Match>()
                                                                    .Select(m => m.Groups[0].Value)
                                                                    .Distinct();
                                        foreach (string match in matchCollection)
                                        {
                                            matchedValue = match.Trim();
                                            if (complexGuardHolder.ContainsKey(matchedValue) && !declaredFunctions.Contains(matchedValue))
                                            {
                                                functionDeclarations = functionDeclarations + "\nbool " + matchedValue + "{\n" + complexGuardHolder[matchedValue] + "return true;\n}";
                                                declaredFunctions.Add(matchedValue);
                                            }
                                        }
                                    }
                                }
                                transitionItem.Label.Add(label);
                            }
                            seperateTransitionList.Add(transitionItem);
                        }
                    }
                    templateName = templateName + "_";
                    template = new Template();
                    template.Name = new Name();
                    template.Declaration = "clock local;\n" + functionDeclarations;
                    template.Name.Text = templateName;
                    template.Init = new Init();
                    template.Init.Ref = locationItem.Id;
                    template.Location = new List<Location>();
                    template.Location.Add(locationItem);
                    template.Transition = new List<Transition>();
                    template.Transition.AddRange(seperateTransitionList);
                    templateList.Add(template);
                    seperateTransitionList.Clear();
                }
                nta.Template = new List<Template>();
                nta.Template.AddRange(templateList);
                nta.Declaration = "clock global;\n" + nta.Declaration;
                string templateNameList = string.Empty;
                foreach (var item in templateList)
                {
                    templateNameList = string.IsNullOrEmpty(templateNameList) ? item.Name.Text : templateNameList + "," + item.Name.Text;
                }
                if (!string.IsNullOrEmpty(templateNameList))
                    nta.System = nta.System + "\n" + "system " + templateNameList + ";";
            }

            #endregion
            XmlSerializer xml = new XmlSerializer(typeof(Nta));
            var xmlnsEmpty = new XmlSerializerNamespaces();
            xmlnsEmpty.Add("", "");
            string fileName = @"//" + sourceFileName + "_" + DateTime.Today.ToLongTimeString().ToString() + ".xml";
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + fileName;
            FileStream file1 = File.Create(path);
            using (XmlWriter writer = XmlWriter.Create(file1))
            {
                //writer.WriteDocType("nta", "-//Uppaal Team//DTD Flat System 1.1//EN\' \'http://www.it.uu.se/research/group/darts/uppaal/flat-1_1.dtd\'", null, null);
                xml.Serialize(writer, nta, xmlnsEmpty);
            }
            file1.Close();
            MessageBox.Show("Converted");
        }

        bool IsComplex(string expression)
        {
            if (!string.IsNullOrEmpty(expression))
            {
                Regex complexRegex = new Regex(Constant.eventToUppalVariable.complexExpressionPattern);
                return complexRegex.IsMatch(expression);
            }
            return false;
        }

        string GetComplexFunctionName(string expression)
        {
            Regex functionRegex = new Regex(Constant.eventToUppalVariable.functionNamePattern);
            if (!string.IsNullOrEmpty(expression))
            {
                Match functionNameMatch = functionRegex.Match(expression);
                if (functionNameMatch.Success)
                    return functionNameMatch.Value;
            }
            return string.Empty;
        }
        string HasStateInVariableName(string carrierSetName, List<ScVariable> scVariables)
        {
            string scVariableName = string.Empty;
            if (!string.IsNullOrEmpty(carrierSetName))
            {
                foreach (ScVariable scVariable in scVariables)
                {
                    if (carrierSetName.Contains(scVariable.Name))
                    {
                        scVariableName = scVariable.Name;
                        break;
                    }
                }
                scVariableName = scVariableName?.ToLower();
                if (Regex.IsMatch(scVariableName, @"_state[0-9]|_states"))
                    return string.Empty;
            }
            return scVariableName;
        }
        public bool IsValidLength(string value)
        {
            if (!string.IsNullOrEmpty(value) && value.Length <= 2 && value.Length >= 0)
                return true;
            return false;
        }

        public bool IsNumeric(string value)
        {
            return value.All(char.IsNumber);
        }

        public int GetNewLocationId(List<int> locationIds)
        {
            return ((locationIds.Count > 0 ? (locationIds.Max() + 1) : 0));
        }

        public string RemoveDuplicate(string input, string addition)
        {
            if (!string.IsNullOrEmpty(input))
            {
                string uniqueItems = string.Empty;
                IEnumerable<string> collection = Regex.Matches(input, @"([^\s]+)")
                    .OfType<Match>()
                    .Select(m => m.Groups[0].Value)
                    .Distinct();
                foreach (string item in collection)
                {
                    uniqueItems = string.IsNullOrEmpty(uniqueItems) ? item : uniqueItems + addition + item;
                }
                return uniqueItems;
            }
            return input;
        }

        public string sourceFileData = string.Empty;
        public string sourceFileName = string.Empty;

        private void button2_Click(object sender, EventArgs e)
        {
            string readfile = string.Empty;
            openFileDialog1.Filter = "bcm files (*.bcm)|*.bcm";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                if (openFileDialog1.CheckPathExists && openFileDialog1.CheckFileExists)
                    if (!string.IsNullOrEmpty(openFileDialog1.FileName))
                    {
                        sourceFileName = openFileDialog1.SafeFileName.Substring(0, openFileDialog1.SafeFileName.IndexOf('.'));
                        readfile = File.ReadAllText(openFileDialog1.FileName);
                        sourceFileData = ReplaceValue(readfile, "org.eventb.core.", "");
                        richTextBox1.Text = sourceFileData;
                    }
            openFileDialog1.Dispose();
        }

        public static string ReplaceValue(string statement, string replaceFrom, string replaceWith)
        {
            return Regex.Replace(statement, string.Format(@"\b{0}\b", replaceFrom), replaceWith);
        }
    }
}

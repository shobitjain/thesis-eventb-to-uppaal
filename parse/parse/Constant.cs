﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parse
{
    public static class Constant
    {
        public static class eventToUppalVariable
        {
            public static string BOOL = "BOOL";
            public static string INT = "ℤ";
            public static string M = "M";
            public static string X = "×";
            public static string P = "ℙ";
            public static string wordMatchPattern = @"\(\w+\)";
            public static string axiomPartitionPattern = @"(?<=\{)\w+(?=\})";
            public static string axiomCarrierSetConstantValuePattern = @"\w+\s*↦\s*[0-9]+";
            public static string complexExpressionPattern = @"⩤|⩥|∖|˜|◁|▷|∪|∩|⇸|⤔|↣|⤀|↠|⤖|⊗|∥|λ|⦂|∀|∅|min|⊆|max|∈|∼|↦|∉|⊂|{";
            public static string functionNamePattern = @"^([a-zA-Z])\w+";
            public static string functionDeclarationpattern = @"\s*fn_\w*\(\)";
        }
    }
}

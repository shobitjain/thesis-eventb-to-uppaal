﻿/* 
 Licensed under the Apache License, Version 2.0

 http://www.apache.org/licenses/LICENSE-2.0
 */
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace Parse
{
    [XmlRoot(ElementName = "scRefinesMachine")]
    public class ScRefinesMachine
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "scTarget")]
        public string ScTarget { get; set; }
        [XmlAttribute(AttributeName = "source")]
        public string Source { get; set; }
    }

    [XmlRoot(ElementName = "scSeesContext")]
    public class ScSeesContext
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "scTarget")]
        public string ScTarget { get; set; }
        [XmlAttribute(AttributeName = "source")]
        public string Source { get; set; }
    }

    [XmlRoot(ElementName = "scAxiom")]
    public class ScAxiom
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "label")]
        public string Label { get; set; }
        [XmlAttribute(AttributeName = "predicate")]
        public string Predicate { get; set; }
        [XmlAttribute(AttributeName = "source")]
        public string Source { get; set; }
        [XmlAttribute(AttributeName = "theorem")]
        public string Theorem { get; set; }
    }

    [XmlRoot(ElementName = "scCarrierSet")]
    public class ScCarrierSet
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "source")]
        public string Source { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "scConstant")]
    public class ScConstant
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "source")]
        public string Source { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "scInternalContext")]
    public class ScInternalContext
    {
        [XmlElement(ElementName = "scAxiom")]
        public List<ScAxiom> ScAxiom { get; set; }
        [XmlElement(ElementName = "scCarrierSet")]
        public List<ScCarrierSet> ScCarrierSet { get; set; }
        [XmlElement(ElementName = "scConstant")]
        public List<ScConstant> ScConstant { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "scInvariant")]
    public class ScInvariant
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "label")]
        public string Label { get; set; }
        [XmlAttribute(AttributeName = "predicate")]
        public string Predicate { get; set; }
        [XmlAttribute(AttributeName = "source")]
        public string Source { get; set; }
        [XmlAttribute(AttributeName = "theorem")]
        public string Theorem { get; set; }
    }

    [XmlRoot(ElementName = "scVariable")]
    public class ScVariable
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "abstract")]
        public string Abstract { get; set; }
        [XmlAttribute(AttributeName = "concrete")]
        public string Concrete { get; set; }
        [XmlAttribute(AttributeName = "source")]
        public string Source { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "scRefinesEvent")]
    public class ScRefinesEvent
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "scTarget")]
        public string ScTarget { get; set; }
        [XmlAttribute(AttributeName = "source")]
        public string Source { get; set; }
    }

    [XmlRoot(ElementName = "scAction")]
    public class ScAction
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "assignment")]
        public string Assignment { get; set; }
        [XmlAttribute(AttributeName = "label")]
        public string Label { get; set; }
        [XmlAttribute(AttributeName = "source")]
        public string Source { get; set; }
    }

    [XmlRoot(ElementName = "scParameter")]
    public class ScParameter
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "source")]
        public string Source { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "scEvent")]
    public class ScEvent
    {
        [XmlElement(ElementName = "scRefinesEvent")]
        public ScRefinesEvent ScRefinesEvent { get; set; }
        [XmlElement(ElementName = "scAction")]
        public List<ScAction> ScAction { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "accurate")]
        public string Accurate { get; set; }
        [XmlAttribute(AttributeName = "convergence")]
        public string Convergence { get; set; }
        [XmlAttribute(AttributeName = "extended")]
        public string Extended { get; set; }
        [XmlAttribute(AttributeName = "label")]
        public string Label { get; set; }
        [XmlAttribute(AttributeName = "source")]
        public string Source { get; set; }
        [XmlElement(ElementName = "scParameter")]
        public List<ScParameter> ScParameter { get; set; }
        [XmlElement(ElementName = "scGuard")]
        public List<ScGuard> ScGuard { get; set; }
    }

    [XmlRoot(ElementName = "scGuard")]
    public class ScGuard
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "label")]
        public string Label { get; set; }
        [XmlAttribute(AttributeName = "predicate")]
        public string Predicate { get; set; }
        [XmlAttribute(AttributeName = "source")]
        public string Source { get; set; }
        [XmlAttribute(AttributeName = "theorem")]
        public string Theorem { get; set; }
    }

    [XmlRoot(ElementName = "scMachineFile")]
    public class ScMachineFile
    {
        [XmlElement(ElementName = "scRefinesMachine")]
        public ScRefinesMachine ScRefinesMachine { get; set; }
        [XmlElement(ElementName = "scSeesContext")]
        public ScSeesContext ScSeesContext { get; set; }
        [XmlElement(ElementName = "scInternalContext")]
        public List<ScInternalContext> ScInternalContext { get; set; }
        [XmlElement(ElementName = "scInvariant")]
        public List<ScInvariant> ScInvariant { get; set; }
        [XmlElement(ElementName = "scVariable")]
        public List<ScVariable> ScVariable { get; set; }
        [XmlElement(ElementName = "scEvent")]
        public List<ScEvent> ScEvent { get; set; }
        [XmlAttribute(AttributeName = "accurate")]
        public string Accurate { get; set; }
        [XmlAttribute(AttributeName = "configuration")]
        public string Configuration { get; set; }
    }
}
## Implementation of model transformations between Event-B and UPPAAL Timed Automata

---

## Hardware requirements

Processor	1 GHz,
RAM	512 MB
Disk space (minimum),	
32-bit	4.5 GB,
64-bit	4.5 GB

---

## Software Requirements
Microsoft Windows/Server OS with .NET Framework 4.6.x onwards,
Visual studio 2017+

---

## Run the Tool from repo

1. git clone https://shobitjain@bitbucket.org/shobitjain/thesis-eventb-to-uppaal.git
2. Open solution in visual studio
3. Clean solution and build solution again
4. Run default project

---

## Run portable tool
1. git clone https://shobitjain@bitbucket.org/shobitjain/thesis-eventb-to-uppaal.git
2. Go to repo and navigate to parse project
3. Navigate to bin folder and execute parse.exe